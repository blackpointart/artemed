$(function() {
  	$('#main-navigation').smartmenus();

  	$('.pratners-carousel').slick({
  		
		dots: false,
		infinite: false,
		autoplay: true,
		speed: 300,
		autoplaySpeed: 5000,
		slidesToShow: 6,
		slidesToScroll: 1,
		responsive: [
		{
		  breakpoint: 1199,
		  settings: {
		    slidesToShow: 5,
		    slidesToScroll: 1
		  }
		},
		{
		  breakpoint: 992,
		  settings: {
		    slidesToShow: 4,
		    slidesToScroll: 1
		  }
		},
		{
		  breakpoint: 768,
		  settings: {
		    slidesToShow: 3,
		    slidesToScroll: 1
		  }
		},
		{
		  breakpoint: 520,
		  settings: {
		    slidesToShow: 2,
		    slidesToScroll: 1
		  }
		},
		{
		  breakpoint: 420,
		  settings: {
		    slidesToShow: 1,
		    slidesToScroll: 1
		  }
		}
		]
	});
});
